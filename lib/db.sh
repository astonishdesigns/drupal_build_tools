#!/bin/bash
###############################################################################
# Name:		db.sh
# Purpose:  Database utility functions
###############################################################################

function create_database() {
	print_action "=== Creating Database ($BUILD_NAME) and DB User ($BUILD_NAME) ==="
	
mysql -u$DBUSER --password=$DBPASS <<eof
	GRANT USAGE ON *.* TO $PROJECT@localhost IDENTIFIED BY "$PROJECT" WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
	CREATE DATABASE IF NOT EXISTS $BUILD_NAME;
	GRANT ALL PRIVILEGES ON *.* TO $PROJECT@localhost;
	FLUSH PRIVILEGES;
eof
}