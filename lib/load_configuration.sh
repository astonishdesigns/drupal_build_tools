#!/bin/bash
###############################################################################
# Name:		load_configuration.sh
# Purpose:	Source the global and project configuration files
###############################################################################

function load_configuration() {
	print_action "=== Loading Configuration ==="
	
	# Load global configuration
	if [[ -f $HOME/dbt.cnf ]]; then
		. $HOME/dbt.cnf
		print_success "Loaded global configuration"
	else
		print_error "Global configuration file (dbt.cnf) was not found in your home folder.\n\tDid you forget to run the install?"	
		exit 1
	fi
	
	
	# Load project configuration
	DIR=$(pwd)
	if [[ -f $DIR/project.cnf ]]; then
		. $DIR/project.cnf
		print_success "Loaded project configuration"
	else
		print_error "Project configuration file (project.cnf) doesn't exist, \n\tor you are not executing this script from the project root."
		
		print_message "Would you like to create a configuration file? [Y/n] \c"
		read SELECTION
		
		if [[ -z "$SELECTION" || $SELECTION == "y" || $SELECTION == "Y" ]]; then
				print_message "What is the name of your project? \c"
				read NAME

				if [[ -z "$NAME" ]]; then
					print_error "ERROR: Project name cannot be empty!"
					exit 1
				else
					cp $TOOLS_DIR/conf/project.cnf $DIR/project.cnf
					sed -i -e "s/<<PROJECT_NAME>>/${NAME}/g" $DIR/project.cnf
					rm $DIR/project.cnf-e

					print_success "Project configured. Re-run build tools."
					exit 0
				fi
		elif [[ $SELECTION == "n" || $SELECTION == "N" ]]; then
			exit 1
		fi
	fi
}
