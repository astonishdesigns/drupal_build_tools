#!/bin/bash
###############################################################################
# Name:		branch.sh
# Purpose:	Get the current git branch
###############################################################################

function get_current_branch() {
	if git_repo_exists; then
		print_action "=== Analyzing branch name ==="
		
		# Get the current branch
		BRANCH_NAME=$(git symbolic-ref -q HEAD)
		BRANCH_NAME=${BRANCH_NAME##refs/heads/}
		BRANCH_NAME=${BRANCH_NAME:-HEAD}
		BRANCH_DIR=${BRANCH_NAME#"feature/"}
		BRANCH_DIR=${BRANCH_DIR#"release/"}
		BRANCH_DIR=${BRANCH_DIR#"hotfix/"}
	fi
}

function get_current_release() {
	if git_repo_exists; then
		print_action "=== Analyzing release name ==="
		
		RELEASE_TAG="`git symbolic-ref HEAD 2> /dev/null | cut -b 12-`-`git log --pretty=format:\"%h\" -1`"
		RELEASE_TAG=${RELEASE_TAG#"feature/"}
		RELEASE_TAG=${RELEASE_TAG#"release/"}
		RELEASE_TAG=${RELEASE_TAG#"hotfix/"}
		RELEASE_NAME=${PROJECT}_$RELEASE_TAG
	fi
}