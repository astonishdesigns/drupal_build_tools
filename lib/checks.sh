#!/bin/bash
###############################################################################
# Name:		chekcs.sh
# Purpose:	Collection of functions that perform tests
###############################################################################

function git_repo_exists() {
	# print_action "=== Checking for repository ==="
	
	if [[ -d .git ]]; then
	  # print_success "Repository exists"
	  return 0
	elif [[ $(git rev-parse --git-dir 2> /dev/null) == ".git" ]]; then
	  # print_success "Repository exists"
	  return 0
	else
		print_error "This script is not running within a git repository"
		exit 1
	fi
}

function in_a_build() {
	print_action "=== Checking if current directory is a build ==="
	
	DIR="$(pwd)"
	
	if [[ $DIR = *builds* ]]; then
		print_success "Inside a build"
		return 0
	elif [[ $DIR = *sandbox* ]]; then
		print_success "Inside a build"
		return 0
	else
		print_success "Not inside a build"
		return 1
	fi
}

function remote_branch_exists() {
	if [[ ! -z "$1" ]]; then
		BRANCH="$1"
	else
		BRANCH="$BRANCH_NAME"
	fi
	
	if [[ -z $(git branch -a | grep remotes/origin/$BRANCH) ]]; then
		print_error "Remote branch does not exist!  Did you forget to publish (push) this branch?"
		return 1
	else
		return 0
	fi
}