#!/bin/bash
###############################################################################
# Name:		install.sh
# Purpose:	Install and configure the build tools scripts
###############################################################################

TOOLS_DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $TOOLS_DIR/lib/print.sh

# TODO: Setup a dbt.cnf configuration file
print_action "=== Configuring Global Configuration ==="
print_message "Copying the configuration template into ($HOME/dbt.cnf)"

cp $TOOLS_DIR/conf/dbt.cnf $HOME
sed -i -e "s%<<TOOLS>>%${TOOLS_DIR}%g" $HOME/dbt.cnf

print_message "\nAnswer the following questions. Press enter to accept the defaults..."

DEFAULT="root"
print_message "\nEnter your root database user password ($DEFAULT): "
read dbpass
if [[ -z "$dbpass" ]]; then
	dbpass=$DEFAULT
fi
sed -i -e "s/<<DBPASS>>/${dbpass}/g" $HOME/dbt.cnf

DEFAULT=`whoami`
print_message "\nEnter the owner for files in your webroot ($DEFAULT): "
read webuser
if [[ -z "$webuser" ]]; then
	webuser=$DEFAULT
fi
sed -i -e "s/<<USER>>/${webuser}/g" $HOME/dbt.cnf

DEFAULT=`whoami`
print_message "\nEnter the group name for files in your webroot ($DEFAULT): "
read webgroup
if [[ -z "$webgroup" ]]; then
	webgroup=$DEFAULT
fi
sed -i -e "s/<<GROUP>>/${webgroup}/g" $HOME/dbt.cnf

# Setup an alias to the primary build script
print_action "=== Configuring Command Aliases ==="
$(sudo ln -s $TOOLS_DIR/drupal_build.sh /usr/local/bin/dbt)